/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cache;

/**
 *
 * @author hp
 */
public class CacheManagerTest {
    
    public CacheManagerTest() {
  }
  public static void main(String[] args) {
      
    String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    CachedObject co = new CachedObject(s, new Long(1234), 1);
    
    CacheManager.putCache(co);
    
    CachedObject o = (CachedObject)CacheManager.getCache(new Long(1234));
    
    if (o == null)
      System.out.println("CacheManagerTestProgram.Main:  FAILURE!  Object not Found.");
    else
      System.out.println("CacheManagerTestProgram.Main:  SUCCESS! " +((String)o.object).toString());
  }
}
