package connect;
import java.io.Serializable;
import java.sql.*;


public class Connexion{
    
    public static Connection connect() throws Exception{
        Connection connect = null;
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connect = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=daogeneric;user=naxy;password=Narindra2.0");
        }catch(Exception e){
            throw e;
        }
        
        return connect;	
    }
    
    public static int beginTransaction(Connection connection) throws Exception
    {
        int result = 0;
        String query = "BEGIN TRANSACTION";

        Statement stmt  = connection.createStatement();
        result = stmt.executeUpdate(query);
        
        stmt.close();
        
        return result;
    }

    public static int commit(Connection connection) throws Exception
    {
        int result = 0;
        String query = "COMMIT";

        Statement stmt  = connection.createStatement();
        result = stmt.executeUpdate(query);
        
        stmt.close();
        
        return result;
    }

    public static int rollback(Connection connection) throws Exception
    {
        int result = 0;
        String query = "ROLLBACK";

        Statement stmt  = connection.createStatement();
        result = stmt.executeUpdate(query);
        
        stmt.close();
        
        return result;
    }
}