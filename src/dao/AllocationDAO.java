/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Date;
import model.Allocation;
import model.BaseModel;
import connect.Connexion;

/**
 *
 * @author Sk
 */
public class AllocationDAO {
    public ArrayList<BaseModel> findAll(int begin,int end) throws Exception{
        ArrayList<BaseModel> list = new ArrayList<>();

        Connection connect = Connexion.connect();
        ResultSet res = null;
        
        try {
            String sql = "select * from allocation ORDER BY RAND() OFFSET " + begin +" ROWS FETCH NEXT "+ end +" ROWS ONLY";
            res = connect.prepareStatement(sql).executeQuery();
            while (res.next()) {
                list.add(new Allocation(res.getInt(1), 
                        res.getInt(2), 
                        res.getDate(3), 
                        res.getDouble(4), 
                        res.getInt(5),
                        res.getInt(6)
                        
                ));
            }
        } catch (Exception e) {
            throw e;

        } finally {
            if(res != null) res.close();
            if (connect != null) connect.close();
        }
        return list;
    }
    
    public static void saveAllocation(int idcommune, int tranche, String daty,double montant,int idDemande) throws Exception
	{
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO allocation VALUES ( ?, ?, ?, ?, ?, ?) ";
		try
		{
			conn = Connexion.connect();
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idcommune);
			ps.setInt(2, tranche);
			ps.setObject(3, new Date(21,12,2018));
			ps.setDouble(4, montant);
			ps.setInt(5, idDemande);			
			ps.executeUpdate();
                        
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}
    
    /*public static void updateAllocation(double montantpaye,double reste,int refclient,int mois) throws Exception
	
     {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "update  paiement set montantpaye=?,reste=?  where refclient=? and mois=?";
		try
		{
			conn = Connect.getconn();
			ps = conn.prepareStatement(sql);
			ps.setDouble(1, montantpaye);
			ps.setDouble(2, reste);
			ps.setInt(3, refclient);
			ps.setInt(4, mois);
			ps.executeUpdate();
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}*/
}
