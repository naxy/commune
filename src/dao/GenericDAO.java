/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import annotation.Table;
import connect.Connexion;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.BaseModel;
import query.Criteria;
import query.Query;
import util.Constant;
import util.Utilitaire;

/**
 *
 * @author itu
 */
public class GenericDAO extends InterfaceDAO{

    public int save(BaseModel model) throws Exception {
        int result = 0;
        Connection connection = null;
        
        try{
            connection = Connexion.connect();
            result = save(model, connection);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection != null)
                connection.close();
        }
   
        return result;
    }

    public int save(BaseModel model, Connection connection) throws Exception {
        int result = 0;
        
        String tableName = Utilitaire.getTableName(model);
        List<Field> fields = Utilitaire.fieldsInTable(model);
        List<String> columnsName = Utilitaire.getColumnsName(fields);
        List<Method> getters = Utilitaire.getGetters(model, fields);
        List<Object> fieldValues = Utilitaire.getGettersMethodValues(model, getters);
        
        String columns = "";
        String values = "";
        int nbfields = fields.size();
        
        for(int i = 0; i < nbfields; i++){
            columns += columnsName.get(i);
            values  += "?";
            
            if(i != nbfields-1){
                columns += ",";
                values  += ",";
            }
        }
        
        String query = "INSERT INTO " + tableName + "(" + columns + ") VALUES (" + values + ")"; 
        PreparedStatement ps = null;
            
        try{
            ps = connection.prepareStatement(query);

            for(int i=0; i < nbfields; i++){
                ps.setObject(i+1, fieldValues.get(i));
            }
            

            result = ps.executeUpdate();
        }
        catch(Exception ex){
            throw ex;
        }
        finally{
            if(ps != null) 
                ps.close();
        }
            
        return result;
    }

    public int updateAll(BaseModel model, Query setter, Query query) throws Exception {
        int result = 0;
        Connection connection = null;
        
        try{
            connection = Connexion.connect();
            result = updateAll(model, setter, query, connection);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection != null)
                connection.close();
        }
   
        return result;
    }

    public int updateAll(BaseModel model, Query seter, Query query, Connection connection)throws Exception{
        int result = 0;
        PreparedStatement state = null;
        
        String sql  = "update " + Utilitaire.getTableName(model);
       
        try{ 
            if(query != Query.Empty){
            
                sql += seter.getPreparedSqlUpdate();
                sql += query.getPreparedSql();

                state = connection.prepareStatement(sql);


                List<Object> lsiteOb = query.getValuesList();
                List<Object> lsitSet = seter.getValuesUpdate();

                for(int i=0;i<lsitSet.size();i++){
                    //System.out.println("teste"+lsitSet.get(i));
                    state.setObject(i+1,lsitSet.get(i));
                }
                for(int i=0;i<lsiteOb.size();i++){
                    //System.out.println("teste"+lsiteOb.get(i));
                   state.setObject(i+lsitSet.size()+1,lsiteOb.get(i));
                }

                Connexion.beginTransaction(connection);
                
                System.out.println(sql);
                result = state.executeUpdate();
                Connexion.commit(connection);
            }
        }catch(Exception ex){
            Connexion.rollback(connection);
            throw ex;
        }
         finally{
            if(state!=null){
                state.close();
            }
        }
        return result;
    }


    public int delete(BaseModel model) throws Exception {
        int result = 0;
        Connection connection = null;
        
        try{
            connection = Connexion.connect();
            result = delete(model, connection);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection != null)
                connection.close();
        }
   
        return result;
    }

    public int delete(BaseModel model, Connection connection) throws Exception {
        int result = 0;
        
        Class modelClass = model.getClass();
        int id = (int)modelClass.getMethod("getId").invoke(model);
        
        String query = "DELETE from " + Utilitaire.getTableName(model) + " WHERE ID = ?";
        PreparedStatement ps = null;
        
        try{
            ps = connection.prepareStatement(query);
            ps.setObject(1, id);
            
            result = ps.executeUpdate();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(ps != null)
                ps.close();
        }
        
        return result;
    }


    public BaseModel findById(int id, BaseModel model) throws Exception {
        BaseModel result = null;
        Connection connection = null;
        
        try{
            connection = Connexion.connect();
            result = findById(id, model, connection);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection != null)
                connection.close();
        }
   
        return result;
    }

    public BaseModel findById(int id, BaseModel model, Connection connection) throws Exception {
        return findById(id, model, 1, Constant.LIMIT_LEVEL, connection);
    }
    
    public BaseModel findById(int id, BaseModel model, int currentLevel, int limit, Connection connection) throws Exception {
        List<BaseModel> list = findAll(new Query(Criteria.where("ID").is(id)), model, currentLevel, limit, connection);
        
        if(list.size() > 0)
            return list.get(0);
        
        return null;
    }


    public List<BaseModel> findAll(BaseModel model) throws Exception {
        return findAll(Query.Empty, model);
    }

    public List<BaseModel> findAll(BaseModel model, Connection connection) throws Exception {
        return findAll(Query.Empty, model, connection);
    }

    public List<BaseModel> findAll(Query query, BaseModel model) throws Exception {
        List<BaseModel> result = null;
        Connection connection = null;
        
        try{
            connection = Connexion.connect();
            result = findAll(query, model, connection);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(connection != null)
                connection.close();
        }
   
        return result;
    }

    public List<BaseModel> findAll(Query query, BaseModel model, Connection connection) throws Exception {
        return findAll(query, model, 1, Constant.LIMIT_LEVEL, connection);
    }

    public List<BaseModel> findAll(Query query, BaseModel model, int currentLevel, int limit, Connection connection) throws Exception {
        List<BaseModel> result = new ArrayList<>();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try{
            Class modelClass = model.getClass();
            String tableName = Utilitaire.getTableName(model);
            
            List<Field> fields       = Utilitaire.fieldsInTable(model);
            List<String> columnsName = Utilitaire.getColumnsName(fields);
            List<Method> setters     = Utilitaire.getSetters(model, fields);
            
                     
            String queryString = "SELECT * FROM " + tableName + " ";
            
            if(query != Query.Empty){             
                queryString += query.getPreparedSql();
            }
            
            //System.out.println(query.getPreparedSql());
            stmt = connection.prepareStatement(queryString);
            
            if(query != Query.Empty){
                List<Object> values = query.getValuesList();       
                
                for(int i = 0; i < values.size(); i++){
                    stmt.setObject(i + 1, values.get(i));
                }
            }
            
            rs = stmt.executeQuery();
            
            BaseModel fetch = null;
            
            while(rs.next()){
                fetch = (BaseModel)modelClass.newInstance();
                
                for(int i = 0; i < fields.size(); i++){
                    // Si l'attribut est un objet héritant de BaseModel (càd clé étrangère)
                    if(fields.get(i).getType().getSuperclass() == BaseModel.class){
                        if(currentLevel <= limit){
                            int id           = (int)rs.getObject(columnsName.get(i));
                            Class fieldClass = Class.forName(fields.get(i).getType().getName());

                            setters.get(i).invoke(fetch, this.findById(id, (BaseModel)fieldClass.newInstance(), currentLevel + 1, Constant.LIMIT_LEVEL, connection));
                        }      
                    }
                    // Si l'attribut est une liste , clè étrangère vers la table
                    else if(Collection.class.isAssignableFrom(fields.get(i).getType())){
                        if(currentLevel <= limit){
                            Class genericListType = Utilitaire.getListGenericType(fields.get(i));
                        
                            if(genericListType.getSuperclass() == BaseModel.class){
                                setters.get(i).invoke(fetch, findAll(new Query(Criteria.where("ID" + tableName).is(fetch.getId())), (BaseModel)genericListType.newInstance(), currentLevel + 1, limit, connection));
                            }
                        }    
                    }
                    else{
                        setters.get(i).invoke(fetch, rs.getObject(columnsName.get(i)));
                    }                       
                }
                
                result.add(fetch);
            }
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(rs != null)
                rs.close();
            
            if(stmt != null)
                stmt.close();
        }
        
        return result;
    }
}
