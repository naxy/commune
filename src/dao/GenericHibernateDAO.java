/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import model.BaseModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;
import util.Utilitaire;

/**
 *
 * @author Tiantsoa
 */
public class GenericHibernateDAO {
    public void save(BaseModel bm) throws Exception{
        Transaction trans = null;
        Session sess = HibernateUtil.getSessionFactory().openSession();
        
        try{
            trans = sess.beginTransaction();
            sess.save(bm);
            
            sess.getTransaction().commit();
        }catch(Exception e){
            if(trans != null) trans.rollback();
            throw e;
        }finally{
            if(sess != null) {
                sess.flush();
                sess.close();
            }
        }
    }
    
    public void update(BaseModel bm,int id) throws Exception{
        Transaction trans = null;
        Session sess = HibernateUtil.getSessionFactory().openSession();
        
        try {
            trans = sess.beginTransaction();
            
            BaseModel model = (BaseModel)sess.get(bm.getClass(),id);
            
            List<Field> fields = Utilitaire.fieldsInTable(bm);
            List<Method> setters = Utilitaire.getSetters(model, fields);
            List<Method> getters = Utilitaire.getGetters(bm, fields);
            List<Object> values  = Utilitaire.getGettersMethodValues(bm, getters);
            
            for(int i = 0; i < values.size(); i++){
                if(setters.get(i).getName().toLowerCase().equals("setID".toLowerCase()))
                    setters.get(i).invoke(model, id);
                else
                    setters.get(i).invoke(model, values.get(i));
            }
            
            sess.update(model);
            
            sess.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trans != null) {
                trans.rollback();
            }
            throw e;
        } finally {
            sess.flush();
            sess.close();
        }
    }
    
    public void delete(BaseModel bm, int id) throws Exception{
        Transaction trans = null;
        Session sess = HibernateUtil.getSessionFactory().openSession();
        
        try{
            trans = sess.beginTransaction();
            BaseModel model = (BaseModel) sess.load(bm.getClass().getName(), id);
            sess.delete(model);
            sess.getTransaction().commit();
        }catch(Exception e){
            if(trans != null) trans.rollback();
            e.printStackTrace();
        }finally{
            if(sess != null){
                sess.flush();
                sess.close();
            }
        }
    } 
    
    public List<BaseModel> findAll(BaseModel bm, int begin, int end) throws Exception{
        List<BaseModel> list =  new ArrayList<BaseModel>();
        Transaction trans = null;
        Session sess =  HibernateUtil.getSessionFactory().openSession();
        
        try{
            trans = sess.beginTransaction();
            Query query = sess.createQuery(" from " + Utilitaire.getTableName(bm)).setCacheable(true);
            
            query.setFirstResult(begin);
            query.setMaxResults(end);
            
            list = query.list();
        }catch(Exception e){
         if(trans != null) trans.rollback();
            throw e;
        }finally{
            if(sess != null){
                sess.flush();
                sess.close();
            }
        }
        return list;
    }
    
    public BaseModel findById(BaseModel bm,int id) throws Exception{
        BaseModel list =  null;
        Transaction trans = null;
        Session sess =  HibernateUtil.getSessionFactory().openSession();
        
        try{
            trans = sess.beginTransaction();
            String command = " from '" + bm.getClass().getName() + "' where id = :id";
            Query query = sess.createQuery(command);
            query.setInteger("id", id);
            list = (BaseModel) query.uniqueResult();
        }catch(Exception e){
            if(trans != null) trans.rollback();
            throw e;
        }finally{
            if(sess != null){
                sess.flush();
                sess.close();
            }
        }
        return list;
    }
}
