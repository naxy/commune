/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.util.List;
import model.BaseModel;
import query.Query;

/**
 *
 * @author itu
 */
public abstract class InterfaceDAO {
    public abstract int save(BaseModel model) throws Exception;
    public abstract int save(BaseModel model, Connection connection) throws Exception;
    public abstract int updateAll(BaseModel model, Query setter, Query query) throws Exception;
    public abstract int updateAll(BaseModel model, Query seter, Query query, Connection connection)throws Exception;
    public abstract int delete(BaseModel model) throws Exception;
    public abstract int delete(BaseModel model, Connection connection) throws Exception;
    public abstract BaseModel findById(int id, BaseModel model) throws Exception;
    public abstract BaseModel findById(int id, BaseModel model, Connection connection) throws Exception;
    public abstract List<BaseModel> findAll(BaseModel model) throws Exception;  
    public abstract List<BaseModel> findAll(BaseModel model, Connection connection) throws Exception;
    public abstract List<BaseModel> findAll(Query query, BaseModel model) throws Exception;
    public abstract List<BaseModel> findAll(Query query, BaseModel model, Connection connection) throws Exception;
}
