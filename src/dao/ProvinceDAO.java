/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.BaseModel;
import model.Province;
import connect.Connexion;

/**
 *
 * @author Sk
 */
public class ProvinceDAO {
    public ArrayList<BaseModel> findAllProvince(int begin, int end) throws Exception{
        ArrayList<BaseModel> list = new ArrayList<>();

        Connection connect = Connexion.connect();
        
        ResultSet res = null;
        try {

            String sql = "select * from province ORDER BY RAND() OFFSET " + begin +" ROWS FETCH NEXT "+ end +" ROWS ONLY";
            res = connect.prepareStatement(sql).executeQuery();
            
            while (res.next()) {
                list.add(new Province(res.getString(1), 
                        res.getInt(2)
                        
                ));
            }
        } catch (Exception e) {
            throw e;

        } finally {
            if(res != null) res.close();
            if (connect != null) connect.close();
        }
        return list;
    }
    
    public void saveProvince(String id,String nom) throws Exception
	{
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO province VALUES (?, ?) ";
		try
		{
			conn = Connexion.connect();
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);
                        ps.setString(2, nom);
			ps.executeUpdate();
                        
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}
    
    public void updateProvince(String nom, String id) throws Exception
	
     {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "update  province set nom=? where id=?";
		try
		{
			conn = Connexion.connect();
			ps = conn.prepareStatement(sql);
			ps.setString(1, nom);
                        ps.setString(1, id);
			ps.executeUpdate();
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}
    
    public void DeleteProvince(String id) throws Exception
	
     {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "delete from  province where id=?";
		try
		{
			conn = Connexion.connect();
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.executeUpdate();
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}
}
