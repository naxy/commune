/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.BaseModel;
import model.Region;
import connect.Connexion;

/**
 *
 * @author Sk
 */
public class RegionDAO {
    public ArrayList<BaseModel> findAllRegion(int begin, int end) throws Exception{
        ArrayList<BaseModel> list = new ArrayList<>();

        Connection connect = Connexion.connect();
        ResultSet res = null;
        try {

            String sql = "select * from region ORDER BY RAND() OFFSET " + begin +" ROWS FETCH NEXT "+ end +" ROWS ONLY";
            res = Connexion.connect().prepareStatement(sql).executeQuery();
            while (res.next()) {
                list.add(new Region(res.getInt(1), 
                        res.getString(2),
                        res.getString(3)                        
                ));
            }
        } catch (Exception e) {
            throw e;

        } finally {
            if(res != null) res.close();
            if (connect != null) connect.close();
        }
        return list;
    }
    
    public void saveRegion(String idp,String nom) throws Exception
	{
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO region(idprovince, nom) VALUES (?, ?) ";
		try
		{
			conn = Connexion.connect();
			ps = conn.prepareStatement(sql);
			ps.setString(1, idp);	
                        ps.setString(2, nom);
			ps.executeUpdate();
                        
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}
    
    public void updateRegion(String nom, String id) throws Exception
	
     {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "update  region set nom=? where id=?";
		try
		{
			conn = Connexion.connect();
			ps = conn.prepareStatement(sql);
			ps.setString(1, nom);
                        ps.setString(2, id);
			ps.executeUpdate();
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}
    
    public void DeleteRegion(String id) throws Exception
	
     {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "delete from  region where id=?";
		try
		{
			conn = Connexion.connect();
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.executeUpdate();
		} catch (Exception e) { 
			throw e;
		} finally { 
			if(ps!=null) ps.close();
			if(conn!=null) conn.close();
		}
	}
}
