/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import dao.GenericDAO;
import dao.GenericHibernateDAO;
import java.util.List;
import model.BaseModel;
import model.Maison;
import model.Personne;
import model.Voiture;

/**
 *
 * @author itu
 */
public class DAOGenerique{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        GenericDAO dao = new GenericDAO();
        GenericHibernateDAO ghDAO = new GenericHibernateDAO();
        try{
            Maison maison = new Maison(3, "Ivato");
            Personne personne = new Personne();
            Voiture voiture = new Voiture();
            //new GenericHibernateDAO().save(maison);
            //Voiture details = new Voiture();  
            
            //dao.updateAll(details, new Query(Update.set("Marque").to("Peugeot 207")), new Query(Criteria.where("ID").is("1")));
            
            //List<BaseModel> list = dao.findAll(details);
            
            /*List<BaseModel> list = dao.findAll(new Query(Criteria.orOperator(Criteria.where("ID").is(1), Criteria.where("ID").is(3)).and("IDPersonne").different("3").order("ID", Criteria.ORDER_DESC)), details);
            //List<BaseModel> list = dao.findAll(new Query(Criteria.where("ID").between(1, 2)), details); */
            List<BaseModel> list = ghDAO.findAll(voiture,0,2);
            
            for(BaseModel model : list){
                System.out.println(model.getId() + ": " + model);
            }
            
            GenericHibernateDAO ghd = new GenericHibernateDAO();
            ghd.update(maison, 3);
        }
        catch(Exception e){
            e.printStackTrace();
        }  
    }  
}
