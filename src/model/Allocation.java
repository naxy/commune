/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Sk
 */
public class Allocation extends BaseModel {
    private int commune;
    private int tranche;
    private Date dateDisponibilite;
    private double montant;
    private int demande;

    public Allocation(int commune, int tranche, Date dateDisponibilite, double montant, int demande, int id) throws Exception {
        super(id);
        this.commune = commune;
        this.tranche = tranche;
        this.dateDisponibilite = dateDisponibilite;
        this.montant = montant;
        this.demande = demande;
    }

    public Allocation(int commune, int tranche, Date dateDisponibilite, double montant, int demande) {
        this.commune = commune;
        this.tranche = tranche;
        this.dateDisponibilite = dateDisponibilite;
        this.montant = montant;
        this.demande = demande;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }  
    
    
    public int getCommune() {
        return commune;
    }

    public int getTranche() {
        return tranche;
    }

    public Date getDateDisponibilite() {
        return dateDisponibilite;
    }

    public int getDemande() {
        return demande;
    }

    public void setCommune(int commune) {
        this.commune = commune;
    }

    public void setTranche(int tranche) {
        this.tranche = tranche;
    }

    public void setDateDisponibilite(Date dateDisponibilite) {
        this.dateDisponibilite = dateDisponibilite;
    }

    public void setDemande(int demande) {
        this.demande = demande;
    }
    
    
}
