/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Id;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author itu
 */
public class BaseModel{
    @Id
    @javax.persistence.Id 
    @javax.persistence.GeneratedValue
    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BaseModel(){}
    public BaseModel(int id){
        this.id = id;
    }
    
    private List listeAtribtut;
    public List getListeAttribut()throws Exception{
        try{
            return this.listeAtribtut;
        }catch(Exception ex){
            throw ex;
        }
    }
    public void addListeAttribut(Object ob)throws Exception{
        try{
            if(ob!=null)
             this.listeAtribtut.add(ob);
        }catch(Exception ex){
            throw ex;
        }
    }
}
