/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Column;
import annotation.Table;
import java.io.Serializable;
import javax.persistence.Cacheable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author Tiantsoa
 */

@Table(name = "Maison", sequence = "idmsn")
@javax.persistence.Entity
@javax.persistence.Table(name = "Maison")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Maison extends BaseModel implements Serializable{
    @javax.persistence.Id 
    @javax.persistence.GeneratedValue
    private int id;
    private String adresse;
      
    public Maison(){}
    public Maison(int id, String adresse) {
        super(id);
        this.adresse = adresse;
    }
    
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    @Override
    public String toString(){
        return "Habite à " + this.adresse;
    }
}
