/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Id;
import annotation.Table;
import annotation.NotInTable;
import java.io.Serializable;
import javax.persistence.Cacheable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author itu
 */

@Table(name = "Personne", sequence = "idprs")
@javax.persistence.Entity
@javax.persistence.Table(name = "Personne")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Personne extends BaseModel implements Serializable{
    @javax.persistence.Id 
    @javax.persistence.GeneratedValue
    private int id;
    private String nom;
    private String prenom;
    private int age;
    
    @NotInTable
    @javax.persistence.Transient
    private double argent;
    
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getArgent() {
        return argent;
    }

    public void setArgent(double argent) {
        this.argent = argent;
    }
    
    public Personne(){}
    public Personne(int id, String nom, String prenom, int age, double argent) {
        super(id);
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.argent = argent;
    }
    
    @Override
    public String toString(){
        return this.nom + " " + this.prenom + " a " + this.age + " ans";
    }
}
