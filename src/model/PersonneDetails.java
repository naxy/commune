/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.NotInTable;
import annotation.Table;
import java.util.List;

/**
 *
 * @author Tiantsoa
 */
@Table(name = "Personne", sequence = "idprs")
public class PersonneDetails extends BaseModel{
    private String nom;
    private String prenom;
    private int age;
    @NotInTable
    private double argent;
    Maison maison;
    List<VoitureDetails> voitures;
    
    public PersonneDetails(){}
    public PersonneDetails(int id, String nom, String prenom, int age, double argent, Maison maison, List<VoitureDetails> voitures) {
        super(id);
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.argent = argent;
        this.maison = maison;
        this.voitures = voitures;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getArgent() {
        return argent;
    }

    public void setArgent(double argent) {
        this.argent = argent;
    }

    public Maison getMaison() {
        return maison;
    }

    public void setMaison(Maison maison) {
        this.maison = maison;
    }

    public List<VoitureDetails> getVoitures() {
        return voitures;
    }

    public void setVoitures(List<VoitureDetails> voitures) {
        this.voitures = voitures;
    }
    
    @Override
    public String toString(){
        String result =  this.nom + " " + this.prenom + " a " + this.age + " ans";
        
        if(this.maison != null)
            result += ", " + maison.toString();
        
        if(this.voitures != null){
            for(VoitureDetails voiture : voitures)
                result += "\n\t- " + voiture.toString();
        }
        
        
        return result;
    }
}
