/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Androy
 */
public class Region extends BaseModel {
    private String nom;
    private String idProvince;
    
    public Region(int id, String n, String idp)throws Exception{
        super(id);
        try{
            this.nom=n;
            this.idProvince=idp;
            this.addListeAttribut(n);
            this.addListeAttribut(idp);
        }catch(Exception ex){
            throw ex;
        }
    }
    public String getNom()throws Exception{
        try{
            return this.nom;
        }catch(Exception ex){
            throw ex;
        }
    }
    public void setNom(String v)throws Exception{
        try{
            if(v!=null)
            this.nom=v;
        }catch(Exception ex){
            throw ex;
        }
    }
    public String getIdProvince()throws Exception{
        try{
            return this.idProvince;
        }catch(Exception ex){
            throw ex;
        }
    }
    public void setIdProvince(String id)throws Exception{
        try{
            if(id!=null)
             this.idProvince=id;
        }catch(Exception ex){
            throw ex;
        }
    }
}
