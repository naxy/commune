/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Column;
import annotation.Table;
import java.io.Serializable;
import javax.persistence.Cacheable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author Tiantsoa
 */
@Table(name = "Voiture", sequence = "idvtr")
@javax.persistence.Entity
@javax.persistence.Table(name = "Voiture")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Voiture extends BaseModel implements Serializable{
    @javax.persistence.Id 
    @javax.persistence.GeneratedValue
    private int id;
    
    @Column(name = "IDPersonne")
    @javax.persistence.Column(name = "IDPersonne")
    private int personne;
    private String marque;

    public Voiture(){}
    public Voiture(int id, int personne, String marque) {
        super(id);
        this.personne = personne;
        this.marque = marque;
    }
    
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public int getPersonne() {
        return personne;
    }

    public void setPersonne(int personne) {
        this.personne = personne;
    }
    
    @Override
    public String toString(){
        return "Possède une voiture " + this.marque;
    }
}
