/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Column;
import annotation.Table;

/**
 *
 * @author Tiantsoa
 */

@Table(name = "Voiture", sequence = "idvtr")
public class VoitureDetails extends BaseModel{
    PersonneDetails personne;
    String marque;

    public VoitureDetails(){}
    public VoitureDetails(PersonneDetails personne, String marque, int id) {
        super(id);
        this.personne = personne;
        this.marque = marque;
    }
    
    public PersonneDetails getPersonne() {
        return personne;
    }

    public void setPersonne(PersonneDetails personne) {
        this.personne = personne;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    @Override
    public String toString(){
        if(this.personne == null)
            return "Voiture de marque " + this.marque;
        
        return "Voiture de marque " + this.marque + " et a pour propriétaire " + this.personne;
    }
}
