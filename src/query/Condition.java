/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package query;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author itu
 */
public class Condition {
    private String comparator;
    private String column;
    private List<Object> values;
    
    public Condition(){
        this.comparator = "";
        this.column     = "";
        this.values     = new ArrayList<>();
    }
    public Condition(String comparator, String column, List<Object> values) {
        this.comparator = comparator;
        this.column = column;
        this.values = values;
    }

    public String getComparator() {
        return comparator;
    }

    public void setComparator(String comparator) {
        this.comparator = comparator;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public List<Object> getValues() {
        return values;
    }

    public void setValues(List<Object> values) {
        this.values = values;
    }


}
