/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package query;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author itu
 */
public class Criteria {
    public static int LIKE_POSITION_NONE = 0;
    public static int LIKE_POSITION_BEFORE = 1;
    public static int LIKE_POSITION_AFTER = 2;
    public static int LIKE_POSITION_BOTH = 3;
    
    public static int ORDER_ASC = 1;
    public static int ORDER_DESC = 2;
    
    boolean hasWhere;
    List<Condition> queries;

    public boolean hasWhere() {
        return hasWhere;
    }

    public void setHasWhere(boolean startWithWhere) {
        this.hasWhere = startWithWhere;
    }
    
    
    public List<Condition> getQueries() {
        return queries;
    }

    public void setQueries(List<Condition> queries) {
        this.queries = queries;
    }
    
    public Criteria(){
        this.hasWhere = false;
        this.queries  = new ArrayList<>();
    }
    
    
    public static Criteria where(String column){
        Criteria criteria = new Criteria();
        criteria.setHasWhere(true);
            
        Condition query = new Condition();
        query.setColumn(column);
        
        criteria.queries.add(query);
        return criteria;
    }
    
    public static Criteria orOperator(Criteria ... criterias){
        Criteria criteria = new Criteria();
        criteria.setHasWhere(true);
            
        Condition query = new Condition();
        
        query.setComparator("OR");
        
        for(Criteria c : criterias){
            query.getValues().add(c);
        }
        
        criteria.queries.add(query);
        return criteria;
    }
    
    public static Criteria orderBy(String column, int groupType){
        Criteria criteria = new Criteria();
        criteria.setHasWhere(false);
        
        Condition query = new Condition();
        query.setComparator("ORDER BY");
        query.getValues().add(column);
        query.getValues().add(groupType);
        
        criteria.queries.add(query);
        return criteria;
    }
    
    public Criteria and(String column){
        Condition query = new Condition();
        query.setColumn(column);
        
        this.queries.add(query);
        return this;
    }
    
    public Criteria is(Object value){
        Condition last = this.getLastQuery();
        
        last.setComparator("=");
        last.getValues().add(value);
        return this;
    }
    
    public Criteria lessThan(Object value){
        Condition last = this.getLastQuery();
        
        last.setComparator("<");
        last.getValues().add(value);
        return this;
    }
    
    public Criteria greaterThan(Object value){
        Condition last = this.getLastQuery();
        
        last.setComparator(">");
        last.getValues().add(value);
        return this;
    }
    
    public Criteria different(Object value){
        Condition last = this.getLastQuery();
        
        last.setComparator("<>");
        last.getValues().add(value);
        return this;
    }
    
    public Criteria or(Criteria ... criteria){
        Condition query = new Condition();
        
        query.setComparator("OR");
        
        for(Criteria c : criteria){
            query.getValues().add(c);
        }
        
        this.queries.add(query);
        return this;
    }
    
    public Criteria between(Object value1, Object value2){
        Condition last = this.getLastQuery();
        
        last.setComparator("BETWEEN");
        last.getValues().add(value1);
        last.getValues().add(value2);
        return this;
    }
    
    public Criteria like(Object value, int likePosition){
        Condition last = this.getLastQuery();
        
        last.setComparator("LIKE");
        last.getValues().add(value);
        last.getValues().add(likePosition);
        return this;
    }
    
    public Criteria in(Object ... values){
        Condition last = this.getLastQuery();
        
        last.setComparator("IN");
        
        for(Object value : values){
            last.getValues().add(value);
        }
        
        return this;
    }
    
    public Criteria order(String column, int groupType){
        Condition query = new Condition();
        query.setComparator("ORDER BY");
        query.setColumn(column);
        query.getValues().add(groupType);
        
        this.queries.add(query);
        return this;
    }
        
    
    public Condition getLastQuery(){
        return this.queries.get(this.queries.size() - 1);
    }
}
