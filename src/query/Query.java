/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package query;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiantsoa
 */
public class Query {
    public static Query Empty = null;
    Criteria criteria;
    Update update;
    
    public Query(Criteria criteria) {
        this.criteria = criteria;
    }
    
    public Query(Update update) {
        this.update = update;
    }
    

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }
    
    
    public String getPreparedSql(Criteria criteria, boolean show) throws Exception{
        String result = "";
        
        if(criteria.hasWhere() && show)
            result += " WHERE 1 < 2 ";
        
        for(Condition condition : criteria.getQueries()){
            if(condition.getComparator() == "OR"){
                if(show)
                    result += " AND ";
                
                int i = 0;
                int length = condition.getValues().size();
                
                result += "(";
                
                for(Object element : condition.getValues()){
                    result += "(1 < 2" + getPreparedSql((Criteria)element, false) + ")";
                    
                    i++;
                    
                    if(i < length)
                        result += " " + condition.getComparator() + " ";
                }
                
                result += ")";
            }
            
            else if(condition.getComparator() == "BETWEEN"){
                result += " AND ";
                result += " " + condition.getColumn() + " " + condition.getComparator() + " ? AND ? ";
            }
            
            else if(condition.getComparator() == "LIKE"){
                result += " AND ";
                result += " " + condition.getColumn() + " " + condition.getComparator() + " ? ";
            }
            
            else if(condition.getComparator() == "IN"){
                result += " AND ";
                result += " " + condition.getColumn() + " " + condition.getComparator() + " (";
                
                int length = condition.getValues().size();
                
                for(int i = 0; i < length; i++){
                    result += "?";
                    
                    if(i + 1 < length){
                        result += ",";
                    }
                }
                
                result += ") ";
            }
            
            else if(condition.getComparator() == "ORDER BY"){
                result += " " + condition.getComparator() + " " + condition.getColumn();
                int type = Integer.parseInt(condition.getValues().get(0).toString());
                
                if(type == Criteria.ORDER_ASC)
                    result += " ASC ";
                
                else if(type == Criteria.ORDER_DESC)
                    result += " DESC ";
            }
            
            else{
                if(condition.getValues().size() == 0)
                    throw new Exception("Valeur manquante pour la comparaison");
                
                result += " AND ";
                result += " " + condition.getColumn() + " " + condition.getComparator() + " ? ";  
            }
        }
        
        return result;
    }
    
    public String getPreparedSql() throws Exception{
        return getPreparedSql(criteria, true);
    }
    
    public List<Object> getValuesList(Criteria criteria) throws Exception{
        List<Object> result = new ArrayList<Object>();
        
        for(Condition condition : criteria.getQueries()){
            if(condition.getComparator() == "OR"){                
                for(Object element : condition.getValues()){
                    result.addAll(getValuesList((Criteria)element));
                }
            }
            
            else if(condition.getComparator() == "BETWEEN" || condition.getComparator() == "IN"){
                result.addAll(condition.getValues());
            }
            
            else if(condition.getComparator() == "LIKE"){
                String value = "";
                
                int likePosition = (int)condition.getValues().get(1);
               
                if(likePosition == Criteria.LIKE_POSITION_BEFORE || likePosition == Criteria.LIKE_POSITION_BOTH)
                    value += "%";
                
                value += condition.getValues().get(0);
                
                if(likePosition == Criteria.LIKE_POSITION_AFTER || likePosition == Criteria.LIKE_POSITION_BOTH)
                    value += "%";
                
                result.add(value);
            }
            
            else if(condition.getComparator().matches("[=<>]*")){
                if(condition.getValues().size() == 0)
                    throw new Exception("Valeur manquante pour la comparaison");
                
                result.add(condition.getValues().get(0));
            }
        }
        
        return result;
    }
    
    public String getPreparedSqlUpdate() throws Exception{
        return getPreparedSqlUpdate(update);
    }
    public String getPreparedSqlUpdate(Update update) throws Exception{
        String result = "";
        
        if(!update.getQueries().isEmpty()){
            result += " set ";
             for(Condition condition : update.getQueries()){
                 result +=condition.getColumn()+condition.getComparator()+" ? ";
             }
        }
        return result;
    }
    public List<Object> getValuesListUpdate(Update update) throws Exception{
        List<Object> result = new ArrayList<Object>();
        for(Condition condition : update.getQueries()){
                    result.add(condition.getValues().get(0));
        }
        return result;
    }
    
    public List<Object> getValuesList() throws Exception{
        return getValuesList(criteria);
    }
    
    public List<Object> getValuesUpdate() throws Exception{
        return getValuesListUpdate(update);
    }
}
