/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import query.Condition;

/**
 *
 * @author Androy
 */
public class Update {
    public HashMap listSet=null;
     List<Condition> queries;
       public List<Condition> getQueries() {
        return queries;
    }
     
    public void setQueries(List<Condition> queries) {
        this.queries = queries;
    }
    public Condition getLastQuery(){
        return this.queries.get(this.queries.size() - 1);
    }
    public Update(){
        this.queries  = new ArrayList<>();
    }
    public static Update set(String col){
         
        Update update = new Update();     
        Condition query = new Condition();
        query.setColumn(col);
        query.setComparator("=");
          
        update.getQueries().add(query);
      
        return update;
    }
    public  Update to(Object value){
        Condition last = this.getLastQuery();
        last.getValues().add(value);
        //up.setQueries();
        return this;
    }
    public Update and(String col){
        Update update = new Update();     
        Condition query = new Condition();
        query.setColumn(" and "+col);
        query.setComparator("=");
        update.queries.add(query);
        return update;
    }
}
