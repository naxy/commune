/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author Tiantsoa
 */
public class HibernateUtil {  
    private static final SessionFactory sessionFactory;
    
    static{
        try{
            String cfgPath = "C:/Users/hp/Documents/NetBeansProjects/Commune/hibernate.cfg.xml";
            File hibernatePropsFile = new File(cfgPath);

            Configuration configuration = new Configuration();
            configuration.configure(hibernatePropsFile);

            StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();


            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        catch(Exception e){
            throw new ExceptionInInitializerError(e);
        }
    }
    
    public static SessionFactory getSessionFactory(){   
        return sessionFactory;
    }
}
