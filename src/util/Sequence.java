package util;

import connect.Connexion;
import java.sql.*;

public class Sequence{

    public static int getNextValue(String seqName) throws Exception{
        int result = 0;
        try{
            Connection c    = Connexion.connect();
            Statement stmt  = c.createStatement();
            ResultSet rs    = stmt.executeQuery("select val = next value for "+seqName);

            while(rs.next())
                result = rs.getInt(1);

            rs.close();
            stmt.close();
            c.close();

        }catch(Exception e){
            throw e;
        }
        return result;
    }
    public static int getCurrentValue(String seqName) throws Exception{
        int result = 0;
        try{
            Connection c    = Connexion.connect();
            Statement stmt  = c.createStatement();
            ResultSet rs    = stmt.executeQuery("select cast(current_value as int) as val from sys.sequences where name='"+seqName+"'");
            
            while(rs.next())
                result = rs.getInt(1);

            rs.close();
            stmt.close();
            c.close();

        }catch(Exception e){
            throw e;
        }
        return result;
    }
    public static String getFullNext(String id, String seqName,int length) throws Exception{
        String result = null;
        int value     = Sequence.getNextValue(seqName);

        if(value == -1)
            return result;

        String valueToString = String.valueOf(value);
        int valueLength      = valueToString.length();

        result = id;
        while(valueLength < length){
            result += "0";
            valueLength++;
        }
        result += valueToString;

        return result;
    }
    public static String getFullCurrent(String id, String seqName,int length) throws Exception{
        String result  = null;
        int value      = Sequence.getCurrentValue(seqName);

        if(value == -1)
            return result;

        String valueToString = String.valueOf(value);
        int valueLength      = valueToString.length();

        result = id;
        while(valueLength < length){
            result += "0";
            valueLength++;
        }
        result += valueToString;

        return result;
    }
}


