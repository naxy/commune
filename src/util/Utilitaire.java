/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import annotation.Column;
import annotation.NotInTable;
import annotation.Table;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import model.BaseModel;

/**
 *
 * @author itu
 */
public class Utilitaire {
    public static String getTableName(BaseModel model){
        Table table = model.getClass().getAnnotation(Table.class);
        
        if(table == null || table.name() == "")
            return model.getClass().getSimpleName();
        
        return table.name();
    }
    
    public static String getColumnName(Field field){
        Column column = field.getAnnotation(Column.class);
        
        if(column != null)
            return column.name();
        
        if(field.getType().getSuperclass() == BaseModel.class)
            return "ID" + field.getName();
        
        return field.getName();
    }
    
    public static List<String> getColumnsName(List<Field> fields){
        List<String> result = new ArrayList<>();
        
        for(Field field : fields){
            result.add(Utilitaire.getColumnName(field));
        }
        
        return result;
    }
    
    public static List<Field> fieldsInTable(BaseModel model) throws Exception{
        List<Field> result = new ArrayList<>();
        
        Class modelClass = model.getClass();
        
        if(modelClass.getSuperclass() == BaseModel.class);
            result.add(modelClass.getSuperclass().getDeclaredField("id"));
        
        Field[] fields = model.getClass().getDeclaredFields();
        Annotation[] annotations = null;
        boolean add = true;
        
        for(Field field : fields){
            annotations = field.getAnnotations();
            
            for(Annotation annotation : annotations){
                if(annotation.annotationType() == NotInTable.class)
                    add = false;
            }
            
            if(add){
                result.add(field);
            }
            
            add = true;
        }
     
        return result;
    }
    
    public static ArrayList<Method> getSetters(BaseModel model, List<Field> fields) throws Exception{
        Class modelClass = model.getClass();
       
        ArrayList<Method> result = new ArrayList<>();
        
        for(Field field : fields){
            result.add(modelClass.getMethod("set" + Utilitaire.firstCharToUpper(field.getName()), field.getType()));
        }
        
        return result;
    }
    
    public static ArrayList<Method> getGetters(BaseModel model, List<Field> fields) throws Exception{
        Class modelClass = model.getClass();
       
        ArrayList<Method> result = new ArrayList<>();
        
        for(Field field : fields){
            result.add(modelClass.getMethod("get" + Utilitaire.firstCharToUpper(field.getName())));
        }
        
        return result;
    }
    
    public static ArrayList<Object> getGettersMethodValues(BaseModel model, List<Method> getters) throws Exception{
        ArrayList<Object> result = new ArrayList<>();
        
        for(Method method : getters){
            result.add(method.invoke(model));
        }
        
        return result;
    }
    
    public static Class getListGenericType(Field field){
        return (Class)((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
    }
    
    
    
    public static String firstCharToUpper(String fieldName){
        String firstChar = fieldName.substring(0, 1);
        String end = fieldName.substring(1);
        
        return firstChar.toUpperCase().concat(end);
    }
}
