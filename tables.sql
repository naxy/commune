create table Maison(
	ID int identity(1,1) primary key,
	Adresse varchar(50)
);

create table Personne(
	ID int identity(1,1) primary key,
	IDMaison int,
	Nom varchar(25),
	Prenom varchar(25),
	Age tinyint,
	foreign key(IDMaison) references Maison(ID)
);

create table Voiture(
	ID int identity(1,1) primary key,
	IDPersonne int,
	Marque varchar(25),
	foreign key(IDPersonne) references Personne(ID)
);

insert into Maison(Adresse) values('III A');
insert into Maison(Adresse) values('II B');
insert into Maison(Adresse) values('I C');

insert into Personne(IDMaison, Nom, Prenom, Age) values(1, 'Rabe', 'Soa', 20);
insert into Personne(IDMaison, Nom, Prenom, Age) values(2, 'Rakoto', 'Lita', 25);
insert into Personne(IDMaison, Nom, Prenom, Age) values(3, 'Rahery', 'Bozy', 27);

insert into Voiture(IDPersonne, Marque) values(1, 'Peugeot 206');
insert into Voiture(IDPersonne, Marque) values(1, 'BMW X5');
insert into Voiture(IDPersonne, Marque) values(3, 'VW Golf');